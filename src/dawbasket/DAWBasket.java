/*
 * Ejemplo de Alfredo, sin persistencia y con relación de muchos a uno.
 */
package dawbasket;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mfontana
 */
public class DAWBasket {

    private static List<Equipo> todosEquipos;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        todosEquipos = new ArrayList<>();
        // instanciamos un equipo
        Equipo joventut = new Equipo("Joventut", "Badalona", new Date());
        todosEquipos.add(joventut);
        // instanciamos un jugador
        Jugador epi = new Jugador("Epi", new Date(), 2, 10, 5, "pivot");

//        joventut.getJugadores().add(epi);
// Antes de añadir el jugador al equipo debo asegurarme de que exista
//        boolean existe = false;
//        for (Jugador j : joventut.getJugadores()) {
//            if (j.getIdentificador() == epi.getIdentificador()) {
//                existe = true;
//            }
//        }
        boolean existe = joventut.getJugadores().contains(epi);
        if (!existe) {
            joventut.getJugadores().add(epi);
        }
        Jugador blas = new Jugador("blas", new Date(), 4, 6, 10, "base");
        Jugador mesi = new Jugador("mesi", new Date(), 10, 2, 8, "ala-pivot");
        joventut.getJugadores().add(blas);
        joventut.getJugadores().add(mesi);
//        jugandoConClone(blas);
        buscarJugadoresPorNombre("e");
        
    }
    
    private static void buscarJugadoresPorNombre(String nombre) {
        for (Equipo e : todosEquipos) {
            for (Jugador j : e.getJugadores()) {
                // Problema de Mayúsculas y minúsculas
                String nombreCompleto = j.getNombre().toLowerCase();
                String nombreMinusculas = nombre.toLowerCase();
                // Miramos si contiene una parte del nombre
                if (nombreCompleto.contains(nombreMinusculas)) {
                    System.out.println(j);
                }
            }
        }
    }
    
    
    private static void jugandoConClone(Jugador blas) {
       String clase = blas.getClass().getName();
        System.out.println(clase);
        try {
            Jugador copia = (Jugador) blas.clone();
            System.out.println("Blas");
            System.out.println(blas);
            System.out.println("Copia");
            System.out.println(copia);
            if (copia.equals(blas)) {
                System.out.println("Son iguales");
            } else {
                System.out.println("No son iguales");
            }
            copia.setIdentificador(8);
            System.out.println("Blas");
            System.out.println(blas);
            System.out.println("Copia");
            System.out.println(copia);
             if (copia.equals(blas)) {
                System.out.println("Son iguales");
            } else {
                System.out.println("No son iguales");
            }
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(DAWBasket.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

}
